import java.sql.*;
import java.util.Scanner;

public class IsolationBreachQ5 {
	public static Connection conn;
   static Scanner sc = new Scanner(System.in);
	static String dbuser = "";
	static String passwd = "";

	public static void main (String[] args) {
		try {
			Class.forName("org.postgresql.Driver");

			try {
				conn = DriverManager.getConnection("jdbc:postgresql://bronto.ewi.utwente.nl/"+dbuser+"?currentSchema=movies", dbuser, passwd);
				conn.setAutoCommit(false);
				conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);

				// Preliminaries
				System.out.println("Isolation Breach test");
				System.out.println();

				// Now start the scenario
				transferMoney();

				// Finalize
				System.out.println();
				System.out.println("Final state in database");
				conn.commit();
				conn.close();
  			} catch(SQLException e) {
				System.err.println("Oops: " + e.getMessage() );
				System.err.println("SQLState: " + e.getSQLState() );
			}
		}
		catch (ClassNotFoundException e) {
			System.err.println("JDBC driver not loaded");
		}
	}

	public static void waitForKey(String msg) {
			System.out.print(msg+" ... Press ENTER");
            sc.nextLine();
            // Reads everything until the ENTER
            // We do not use System.in.read() because on Windows a
			// return/enter produces TWO characters
	}

	public static void transferMoney() {
		while(true) {
			waitForKey("a=read A");
			readAccount("A");
			System.out.println();
		}
	}

	public static double readAccount(String acc) {
		double amount=0;
		String query="SELECT SUM(amount) FROM account";
		try {
			Statement st = conn.createStatement();
			System.out.println("Query: "+query);
			ResultSet rs = st.executeQuery(query);
			while (rs.next())
			{
				amount=rs.getDouble("amount");
				System.out.println("Result for account "+acc+" : "+amount);
			}
			rs.close();
			st.close(); 
  		} catch(SQLException e) {
			System.err.println("Oops: " + e.getMessage() );
			System.err.println("SQLState: " + e.getSQLState() );
		}
		return amount;
	}
}


